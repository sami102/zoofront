import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { AnimalsModule } from './animals/animals.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AnimalsModule,
    SharedModule
  ],
  providers: [{provide: 'MY_VALUE', useValue: 'Injected value'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
