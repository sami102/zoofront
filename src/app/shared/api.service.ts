import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

export abstract class ApiService {


    protected constructor(
        private readonly host: string,
        private readonly httpClient: HttpClient) { }

    protected get<T>(url: string): Observable<T> {
        return this.request('GET', url, null);
    }

    protected post<T>(url: string, body: any): Observable<T> {
        return this.request('POST', url, body);
    }

    protected put<T>(url: string, body: any): Observable<T> {
        return this.request('PUT', url, body);
    }

    protected delete<T>(url: string): Observable<T> {
        return this.request('DELETE', url, null);
    }

    private request<T>(method: string, url: string, body: any | null): Observable<T> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            }),
            body,
            params: {}
        };

        return this.httpClient.request<T>(method, `${this.host}/${url}`, httpOptions);
    }
}
