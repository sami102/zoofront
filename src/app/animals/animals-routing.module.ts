import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnimalsListComponent } from './animals-list/animals-list.component';
import { AnimalsDetailsComponent } from './animals-details/animals-details.component';


const routes: Routes = [
  {path: 'animals', component: AnimalsListComponent},
  {path: 'animals/:id', component: AnimalsDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnimalsRoutingModule { }
