import { Component, OnInit } from '@angular/core';
import { AnimalService } from '../shared/animal.service';
import { Animal } from '../shared/animal.model';

@Component({
  selector: 'app-animals-list',
  templateUrl: './animals-list.component.html',
  styleUrls: ['./animals-list.component.css']
})
export class AnimalsListComponent implements OnInit {

  constructor(private readonly service: AnimalService) { }

  animalsList: Animal[];
  newAnimalName: string;
  animal: Animal = new Animal();
  public selectedName: string;
  searchingByName = '';

  ngOnInit() {
    this.searchForAnimals();
  }


  public searchForAnimals(): void {
    this.service.searchAnimals(`?Name=${this.searchingByName}`).subscribe(animals => this.animalsList = animals);
  }

  public async createAnimal(): Promise<void> {
    if (this.newAnimalName.length > 2) {

      this.animal.name = this.newAnimalName;

      await this.service.addAnimal(this.animal).toPromise();

      this.animal = new Animal();

      await this.searchForAnimals();
    } else {
      alert('Please insert name longer than 2 characters');
      this.newAnimalName = '';
    }
  }

  public async deleteAnimal(animal: Animal): Promise<void> {
    const isConfirmed = confirm('Are you sure?');
    if (!isConfirmed){
      return;
    }
    await this.service.removeAnimal(animal).toPromise();

    this.searchingByName = '';
    await this.searchForAnimals();
  }

  public highlightRow(animal) : void {
    this.selectedName = animal.id;
  }

  public animalWasSelected(animal: Animal) : void {
    console.log(animal);
  }

}
