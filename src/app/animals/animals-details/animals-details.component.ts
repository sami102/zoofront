import { Component, OnInit } from '@angular/core';
import { Animal } from '../shared/animal.model';
import { ActivatedRoute, Router } from '@angular/router';
import { AnimalService } from '../shared/animal.service';

@Component({
  selector: 'app-animals-details',
  templateUrl: './animals-details.component.html',
  styleUrls: ['./animals-details.component.css']
})
export class AnimalsDetailsComponent implements OnInit {
  animalId: string;
  animalName: string;
  animalHabitat;
  animal: Animal;
  habitatTypes: string[] = ['Basic', 'Ocean', 'Desert', 'Jungle', 'Forest']
  selectedType: string;
  updatable: boolean;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly service: AnimalService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.animalId = params.id;
      this.getAnimal();
      this.selectedType = 'Basic';
    });
  }

  public async getAnimal() {
    this.animal = await this.service.getAnimal(this.animalId).toPromise();

  }

  public async navigateToAnimalList(){
    this.router.navigate(['/animals']);
  }

  public async updateThisAnimal() {
    this.animal.habitat = this.selectedType;
    
    await this.service.updateAnimal(this.animal).toPromise();
    this.navigateToAnimalList();
  }

  public isHabitatEmpty(): boolean {
    return (this.selectedType === 'Basic');
  }

  public canUpdate(): boolean {
    return (this.isHabitatEmpty());
  }

}
