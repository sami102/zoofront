import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/shared/api.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { Observable } from 'rxjs';
import { Animal } from './animal.model';

@Injectable({
  providedIn: 'root'
})

export class AnimalService extends ApiService {

  constructor(
    httpClient: HttpClient) {
    super(
      environment.apiUrl,
      httpClient);
  }


  getAnimal(id: string): Observable<Animal> {
    return super.get<Animal>(id);
  }

  searchAnimals(match?: string): Observable<Animal[]> {
    return super.get<Animal[]>(match);
  }

  addAnimal(animal: Animal): Observable<any> {
    return super.post('', animal);
  }

  updateAnimal(animal: Animal): Observable<any> {
    return super.put(`${animal.id}`, animal);
  }

  removeAnimal(animal: Animal): Observable<any> {
    return super.delete(`${animal.id}`);
  }
}

