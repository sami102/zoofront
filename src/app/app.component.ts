import { Component, ViewEncapsulation, Inject } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class AppComponent {
  title = 'ZooSami';
  route: string;
  constructor(@Inject('MY_VALUE') injectedValue, private router: Router) {
    router.events.subscribe(() => this.route = router.url);
    console.log(injectedValue);
  }
}
